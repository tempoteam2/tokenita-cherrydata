package main

import "flag"

func RegisterFlags() *Config {
	config := Config{}
	flag.StringVar(&config.Port, "port", ":8081", "The port exposed by application")
	flag.BoolVar(&config.Debug, "debug", true, "If true, debug info will be logging")
	flag.IntVar(&config.Cache.Size, "cache-size", 100*1024*1024, "Cache size in bytes")
	flag.StringVar(&config.Images.BaseURL, "images-base-url", "http://localhost:3001/", "Images base url")
	flag.StringVar(&config.DB.User, "db-user", "", "DB User")
	flag.StringVar(&config.DB.Password, "db-password", "", "DB Password")
	flag.StringVar(&config.DB.Hostname, "db-hostname", "localhost", "DB Hostname")
	flag.StringVar(&config.DB.Name, "db-name", "tokenita", "DB Name")
	flag.StringVar(&config.DB.Port, "db-port", "5432", "DB Port")
	flag.IntVar(&config.DB.MaxIdleConns, "db-maxidle", 50, "DB Max Idle Connections")
	return &config
}

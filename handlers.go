package main

import (
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
)

type Ctx struct {
	S StorageInterface
}

type Error interface {
	error
	Status() int
}

type StatusError struct {
	Code int
	Err  error
}

func (err StatusError) Error() string {
	return err.Err.Error()
}

func (err StatusError) Status() int {
	return err.Code
}

type Handler func(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error

// We use this wrapper to create closure and pass context to every handler
func ToHandlerFunc(ctx *Ctx, fn Handler) rest.HandlerFunc {
	return func(w rest.ResponseWriter, r *rest.Request) {
		err := fn(ctx, w, r)
		if err != nil {
			log.WithFields(log.Fields{
				"module": "handlers",
				"error":  err,
				"url":    r.URL,
			}).Warn("Handler error")
			switch e := err.(type) {
			case Error:
				rest.Error(w, e.Error(), e.Status())
			default:
				rest.Error(
					w,
					http.StatusText(http.StatusInternalServerError),
					http.StatusInternalServerError,
				)
			}
		}
	}
}

// Route: GET - "/v1/status"
func CheckStatus(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	status, err := ctx.S.CheckDBStatus()
	// TODO: check the err type and how it is translated to HTTP code
	if err != nil {
		return err
	}
	w.WriteJson(status)
	return nil
}

// Route: GET - "/v1/tokens"
func GetTokens(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	// TODO: implement validate params function and handle different usecases
	sPage := r.URL.Query().Get("page")
	sPerPage := r.URL.Query().Get("per_page")
	sort := r.URL.Query().Get("sort")
	search := r.URL.Query().Get("search")
	baseCurrency := r.URL.Query().Get("base_currency")

	if sPage == "" {
		sPage = "1"
	}

	if sPerPage == "" {
		sPerPage = "10"
	}

	if sort == "" {
		sort = "symbol"
	}

	// TODO: happy path only
	page, _ := strconv.Atoi(sPage)
	perPage, _ := strconv.Atoi(sPerPage)

	tokens, err := ctx.S.GetTokens(page, perPage, sort, search, baseCurrency)
	// TODO: Move this block to a separate function handle404Error
	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		case *BadRequestError:
			return StatusError{400, err}
		default:
			return err
		}
	}

	w.WriteJson(&tokens)
	return nil
}

// Route: GET - "/v1/tokens/:symbol"
func GetTokenBySymbol(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	symbol := r.PathParam("symbol")
	baseCurrency := r.URL.Query().Get("base_currency")

	if baseCurrency == "" {
		baseCurrency = "usd"
	}

	token, err := ctx.S.GetTokenBySymbol(symbol, baseCurrency)

	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		case *BadRequestError:
			return StatusError{400, err}
		default:
			return err
		}
	}

	w.WriteJson(&token)
	return nil
}

// Route: GET - "/v1/icos"
func GetICOs(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	// TODO: implement validate params function and handle different usecases
	sPage := r.URL.Query().Get("page")
	sPerPage := r.URL.Query().Get("per_page")
	sort := r.URL.Query().Get("sort")
	search := r.URL.Query().Get("search")
	icoType := r.URL.Query().Get("type")
	baseCurrency := r.URL.Query().Get("base_currency")

	if sPage == "" {
		sPage = "1"
	}

	if sPerPage == "" {
		sPerPage = "10"
	}

	if sort == "" {
		sort = "name"
	}

	// TODO: happy path only
	page, _ := strconv.Atoi(sPage)
	perPage, _ := strconv.Atoi(sPerPage)

	icos, err := ctx.S.GetICOs(page, perPage, sort, search, icoType, baseCurrency)
	// TODO: Move this block to a separate function handle404Error
	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		case *BadRequestError:
			return StatusError{400, err}
		default:
			return err
		}
	}

	w.WriteJson(&icos)
	return nil
}

// Route: GET - "/v1/tokens/:symbol"
func GetICOByName(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	name := r.PathParam("name")
	baseCurrency := r.URL.Query().Get("base_currency")

	ico, err := ctx.S.GetICOByName(name, baseCurrency)

	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		case *BadRequestError:
			return StatusError{400, err}
		default:
			return err
		}
	}

	w.WriteJson(ico)
	return nil
}

// Route: GET - "/v1/exchanges"
func GetExchanges(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	// TODO: implement validate params function and handle different usecases
	sPage := r.URL.Query().Get("page")
	sPerPage := r.URL.Query().Get("per_page")
	sort := r.URL.Query().Get("sort")
	search := r.URL.Query().Get("search")
	baseCurrency := r.URL.Query().Get("base_currency")

	if sPage == "" {
		sPage = "1"
	}

	if sPerPage == "" {
		sPerPage = "10"
	}

	if sort == "" {
		sort = "name"
	}

	// TODO: happy path only
	page, _ := strconv.Atoi(sPage)
	perPage, _ := strconv.Atoi(sPerPage)

	exchanges, err := ctx.S.GetExchanges(page, perPage, sort, search, baseCurrency)

	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		case *BadRequestError:
			return StatusError{400, err}
		default:
			return err
		}
	}

	w.WriteJson(exchanges)
	return nil
}

// Route: GET - "/v1/exchanges/:name"
func GetExchangeByName(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	name := r.PathParam("name")
	baseCurrency := r.URL.Query().Get("base_currency")

	exchange, err := ctx.S.GetExchangeByName(name, baseCurrency)

	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		default:
			return err
		}
	}

	w.WriteJson(exchange)
	return nil
}

// Route: GET - "/v1/exchanges/:name"
func GetExchangePairs(ctx *Ctx, w rest.ResponseWriter, r *rest.Request) error {
	name := r.PathParam("name")
	baseCurrency := r.URL.Query().Get("base_currency")
	sort := r.URL.Query().Get("sort")
	search := r.URL.Query().Get("search")

	if sort == "" {
		sort = "-volume_24h"
	}

	pairs, err := ctx.S.GetExchangePairs(name, sort, search, baseCurrency)

	if err != nil {
		switch err.(type) {
		case *DataNotFoundError:
			return StatusError{404, err}
		default:
			return err
		}
	}

	w.WriteJson(pairs)
	return nil
}

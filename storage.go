package main

import (
	"database/sql"
	"fmt"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/coocood/freecache"
	_ "github.com/lib/pq"
)

type StorageInterface interface {
	CheckDBStatus() (*Status, error)
	GetTokens(page, perPage int, sort string, search string, baseCurrency string) (*ShortTokens, error)
	GetTokenBySymbol(sym string, baseCurrency string) (*TokenDetails, error)
	GetExchanges(page, perPage int, sort string, search string, baseCurrency string) (*Exchanges, error)
	GetExchangeByName(name string, baseCurrency string) (*ExchangeDetails, error)
	GetExchangePairs(name string, sort string, search string, baseCurrency string) (*ExchangePairs, error)
	GetICOs(page, perPage int, sort string, search string, filterByType string, baseCurrency string) (*ShortICOs, error)
	GetICOByName(icoName string, baseCurrency string) (*ICODetails, error)
}

type Storage struct {
	db     *sql.DB
	config *Config
	cache  *freecache.Cache
}

type BadRequestError struct {
	message string
}

func (e BadRequestError) Error() string {
	return e.message
}

type DataNotFoundError struct {
	Err    error
	Reason string
}

func (e DataNotFoundError) Error() string {
	return e.GetReason()
}

func (e DataNotFoundError) GetReason() string {
	return e.Reason
}

func handleSQLError(e error, reason string) error {
	switch {
	case e == sql.ErrNoRows:
		return &DataNotFoundError{e, reason}
	default:
		log.WithFields(log.Fields{
			"module": "storage",
			"error":  e,
			"reason": reason,
		}).Error("Storage error")
		return e
	}
}

func InitStorage(config *Config) (StorageInterface, error) {
	dbConfig := config.DB
	cacheConfig := config.Cache
	datasource := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Hostname,
		dbConfig.Port,
		dbConfig.Name)

	conn, err := sql.Open("postgres", datasource)
	if err != nil {
		return nil, err
	}

	log.WithFields(log.Fields{
		"module": "storage",
	}).Info("Connection to storage established")

	// TODO: I have to ping DB because Open for some reason doesn't return error
	// even if DB is not up and running. However in all tutorials there is nothing
	// more than Open.
	if err := conn.Ping(); err != nil {
		return nil, err
	}

	conn.SetMaxIdleConns(dbConfig.MaxIdleConns)
	cache := freecache.NewCache(cacheConfig.Size)
	storage := &Storage{conn, config, cache}
	return storage, nil
}

type Status struct {
	Status string `json:"status"`
}

func (s *Storage) CheckDBStatus() (*Status, error) {
	// TODO: Check how it works because it seem that if start PG, start cherrydata
	// then drop PG the status remains OK.
	if err := s.db.Ping(); err != nil {
		return nil, err
	}
	return &Status{"ok"}, nil
}

type Meta struct {
	Page         int    `json:"page"`
	PerPage      int    `json:"per_page"`
	Count        int    `json:"count"`
	Sort         string `json:"sort"`
	Search       string `json:"search"`
	BaseCurrency string `json:"base_currency"`
}

type ICOMeta struct {
	Page         int    `json:"page"`
	PerPage      int    `json:"per_page"`
	Count        int    `json:"count"`
	Sort         string `json:"sort"`
	Search       string `json:"search"`
	BaseCurrency string `json:"base_currency"`
}

type DetailsMeta struct {
	BaseCurrency string `json:"base_currency"`
}

type SortParam struct {
	Name string
	// TODO check how to create own types with limited space of possible values
	Direction string
}

func validateBaseCurrency(baseCurrency string) (string, error) {
	if baseCurrency == "" {
		baseCurrency = "usd"
	}
	if baseCurrency != "usd" && baseCurrency != "btc" && baseCurrency != "eth" {
		return "", &BadRequestError{fmt.Sprintf("Invald base currency %s", baseCurrency)}
	}
	return baseCurrency, nil
}

func validateSortParam(validParams []string, sort string) (*SortParam, error) {
	for _, v := range validParams {
		if sort == v {
			return &SortParam{v, "ASC"}, nil
		}
		if sort == fmt.Sprintf("-%s", v) {
			return &SortParam{v, "DESC"}, nil
		}
	}
	return nil, &BadRequestError{fmt.Sprintf("Invalid sort param value: %s", sort)}
}

func (s *Storage) getLinksFor(entity string, ids []string) (map[int][]Link, error) {
	result := map[int][]Link{}
	if len(ids) == 0 {
		return result, nil
	}

	var (
		linkType, title, url string
		entityID             int
	)
	query := fmt.Sprintf(`
		SELECT
		  type, %s_id, title, url
		FROM %s_links
		WHERE %s_id IN (%s)
	`, entity, entity, entity, strings.Join(ids, ","))

	linksRows, err := s.db.Query(query)

	if err != nil {
		return nil, err
	}

	for i := 0; linksRows.Next(); i++ {
		linksRows.Scan(
			&linkType,
			&entityID,
			&title,
			&url,
		)
		result[entityID] = append(result[entityID], Link{linkType, title, url})
	}
	return result, nil
}

type Link struct {
	Type  string `json:"type"`
	Title string `json:"title"`
	URL   string `json:"url"`
}

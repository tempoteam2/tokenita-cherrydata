
# cherrydata
Skinny API layer for api.tokenstatistics.com

## Installation instructions
1. [Install Go](https://golang.org/doc/install)
2. Install Godep using the following command: `go get github.com/tools/godep`
3. Add Go binaries to the PATH using profile: `export PATH=$PATH:$(go env GOPATH)/bin`
4. In directory $GOPATH/src clone this repository: `git clone git@github.com:tokenstatistics/cherrydata.git`
5. `cd cherrydata`
6. Install all dependencies: `godep restore`
7. Compile all the things: `go build`
8. Run the binary: `../../bin/cherrydata`

## Deps management
To add a new dep you should:
1. Run `go get <dependency>`
2. Run `godep save`
3. Do not forget to commit `Godeps.json` and `vendor` directory

## PostgreSQL installation (MacOSX)
1. brew update
2. brew install postgresql
3. postgres -D /usr/local/var/postgres

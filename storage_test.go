package main

import (
	"testing"

	"github.com/coocood/freecache"
)

var testConfig = Config{
	"8080",
	false,
	DB{"user", "password", "hostname", "name", "3306", 10},
	Images{"http://example.com/"},
	Cache{
		Size: 100 * 1024 * 1024,
	},
}

var testCache = freecache.NewCache(testConfig.Cache.Size)

func TestValidateSortParam(t *testing.T) {
	type validTestCase struct {
		ValidParams []string
		Sort        string
		Result      *SortParam
	}

	type invalidTestCase struct {
		ValidParams  []string
		Sort         string
		ErrorMessage string
	}

	validTestCases := []validTestCase{
		validTestCase{
			[]string{"sortParam1", "sortParam2"},
			"sortParam1",
			&SortParam{"sortParam1", "ASC"},
		},
		validTestCase{
			[]string{"sortParam1", "sortParam2"},
			"-sortParam2",
			&SortParam{"sortParam2", "DESC"},
		},
	}

	for _, test := range validTestCases {
		result, _ := validateSortParam(test.ValidParams, test.Sort)

		if result.Direction != test.Result.Direction || result.Name != test.Result.Name {
			t.Errorf("\n...expected: %v\n...got: %v", test.Result, result)
		}
	}

	invalidTestCases := []invalidTestCase{
		invalidTestCase{
			[]string{"param1", "param2", "param3"},
			"invalidParam",
			"Invalid sort param value: invalidParam",
		},
		invalidTestCase{
			[]string{"param1", "param2", "param3"},
			"-param",
			"Invalid sort param value: -param",
		},
	}
	for _, test := range invalidTestCases {
		_, err := validateSortParam(test.ValidParams, test.Sort)

		if err.Error() != test.ErrorMessage {
			t.Errorf("\n...expected: %v\n...got: %v", test.ErrorMessage, err.Error())
		}
	}

}

func TestValidateBaseCurrency(t *testing.T) {
	type testCase struct {
		input        string
		output       string
		errorMessage string
	}
	testCases := []testCase{
		{"btc", "btc", ""},
		{"eth", "eth", ""},
		{"usd", "usd", ""},
		{"ltc", "", "Invald base currency ltc"},
	}
	for _, tCase := range testCases {
		result, err := validateBaseCurrency(tCase.input)
		if result != tCase.output {
			t.Errorf("\n...expected: %v\n...got: %v", tCase.output, result)
		}
		if err != nil && err.Error() != tCase.errorMessage {
			t.Errorf("\n...expected: %v\n...got: %v", tCase.errorMessage, err)
		}
		if tCase.errorMessage != "" && err == nil {
			t.Errorf("\n...expected: %v\n...got: %v", tCase.errorMessage, nil)
		}
	}
}

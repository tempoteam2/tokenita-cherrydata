package main

import "testing"

func TestGetLinksForTokens(t *testing.T) {
	// TODO cover only one use-case here (empty ids array should result in empty map).
	// Need to mock db.Query here and check that right sql query were send
	// https://github.com/tokenstatistics/cherrydata/issues/28

	testStorage := Storage{nil, &testConfig, testCache}
	result, err := testStorage.getLinksFor("tokens", []string{})
	if err != nil {
		t.Errorf("\n...expected: nil, got: %v", err)
	}
	if len(result) != 0 {
		t.Errorf("\n...expected: empty map, got: %v", result)
	}
}

func TestNormalizeImage(t *testing.T) {
	testStorage := Storage{nil, &testConfig, testCache}
	testCases := map[NullString]NullString{
		{Valid: true, String: "img.png"}:       {Valid: true, String: "http://example.com/img.png"},
		{Valid: true, String: "img_small.png"}: {Valid: true, String: "http://example.com/img_small.png"},
		{Valid: false, String: ""}:             {Valid: false, String: ""},
	}
	for input, output := range testCases {
		result := testStorage.normalizeImage(input)
		if result.Valid != output.Valid || result.String != output.String {
			t.Errorf("\n...expected: %v, got: %v", output, result)
		}
	}
}

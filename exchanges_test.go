package main

import "testing"

func TestGetLinksForExchanges(t *testing.T) {
	// TODO cover only one use-case here (empty ids array should result in empty map).
	// Need to mock db.Query here and check that right sql query were send
	// https://github.com/tokenstatistics/cherrydata/issues/28

	testStorage := Storage{nil, &testConfig, testCache}
	result, err := testStorage.getLinksFor("exchanges", []string{})
	if err != nil {
		t.Errorf("\n...expected: nil, got: %v", err)
	}
	if len(result) != 0 {
		t.Errorf("\n...expected: empty map, got: %v", result)
	}
}

package main

import (
	"github.com/ant0ine/go-json-rest/rest"
)

// More information about the type here:
// https://godoc.org/github.com/ant0ine/go-json-rest/rest#Route
func GetRoutes(ctx *Ctx) []*rest.Route {
	return []*rest.Route{
		&rest.Route{
			"GET",
			"/v1/status",
			ToHandlerFunc(ctx, CheckStatus),
		},
		&rest.Route{
			"GET",
			"/v1/tokens",
			ToHandlerFunc(ctx, GetTokens),
		},
		&rest.Route{
			"GET",
			"/v1/tokens/:symbol",
			ToHandlerFunc(ctx, GetTokenBySymbol),
		},
		&rest.Route{
			"GET",
			"/v1/exchanges",
			ToHandlerFunc(ctx, GetExchanges),
		},
		&rest.Route{
			"GET",
			"/v1/exchanges/:name",
			ToHandlerFunc(ctx, GetExchangeByName),
		},
		&rest.Route{
			"GET",
			"/v1/exchanges/:name/pairs",
			ToHandlerFunc(ctx, GetExchangePairs),
		},
		&rest.Route{
			"GET",
			"/v1/icos",
			ToHandlerFunc(ctx, GetICOs),
		},
		&rest.Route{
			"GET",
			"/v1/icos/:name",
			ToHandlerFunc(ctx, GetICOByName),
		},
	}
}

package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

const TYPE_FIELD_DEFINITION = `
CASE WHEN start_date > now() THEN 'upcoming'
     WHEN end_date < now() THEN 'finished'
     ELSE 'ongoing'
END`

const PROGRESS_FIELD_DEFINITION = `
    ROUND(
        (100 * funds_raised / (
            CASE WHEN hard_funds_cap IS NOT NULL THEN hard_funds_cap
                WHEN soft_funds_cap IS NOT NULL THEN soft_funds_cap
                ELSE NULL
            END
        )::numeric)
    , 2)`

const ICOExpiration = time.Duration(365 * 24 * time.Hour)

type ShortICO struct {
	// Id field needs to assign links to ico, we do not need to return it
	ID           int         `json:"-"`
	Name         string      `json:"name"`
	Title        string      `json:"title"`
	Type         string      `json:"type"`
	Progress     NullFloat64 `json:"progress"`
	StartDate    NullTime    `json:"start_date"`
	EndDate      NullTime    `json:"end_date"`
	FundsRaised  NullFloat64 `json:"funds_raised"`
	StartPrice   NullFloat64 `json:"start_price"`
	SoftFundsCap NullFloat64 `json:"soft_funds_cap"`
	HardFundsCap NullFloat64 `json:"hard_funds_cap"`
	Links        []Link      `json:"links"`
}

type ShortICOs struct {
	Meta     ICOMeta    `json:"meta"`
	ShortICO []ShortICO `json:"data"`
}

type ICO struct {
	ID           int         `json:"-"`
	Name         string      `json:"name"`
	Token        string      `json:"title"`
	Type         string      `json:"type"`
	Progress     NullFloat64 `json:"progress"`
	StartDate    NullTime    `json:"start_date"`
	EndDate      NullTime    `json:"end_date"`
	FundsRaised  NullFloat64 `json:"funds_raised"`
	StartPrice   NullFloat64 `json:"start_price"`
	SoftFundsCap NullFloat64 `json:"soft_funds_cap"`
	HardFundsCap NullFloat64 `json:"hard_funds_cap"`
	Description  string      `json:"description"`
	Whitepaper   string      `json:"whitepaper"`
	Links        []Link      `json:"links"`
}

type ICODetails struct {
	DetailsMeta DetailsMeta `json:"meta"`
	ICO         ICO         `json:"data"`
}

func getICOWhere(search string, icoType string, now time.Time) (string, error) {
	if search == "" && icoType == "" {
		return "True", nil
	}

	whereClause := []string{} // Dummy where clause

	if search != "" {
		whereClause = append(whereClause, fmt.Sprintf(`(name ILIKE '%%%s%%')`, search))
	}

	if icoType != "" {
		switch icoType {
		case "upcoming":
			whereClause = append(whereClause, "start_date > now()")
		case "ongoing":
			expirationTime := now.
				Add(-1 * ICOExpiration).
				UTC().
				Format(time.RFC3339)
			whereClause = append(whereClause, fmt.Sprintf("start_date < now() AND (end_date > now() OR end_date IS NULL) AND start_date > '%s'", expirationTime))
		case "finished":
			whereClause = append(whereClause, "end_date < now()")
		default:
			return "", &BadRequestError{fmt.Sprintf("Invald ICO type %s", icoType)}
		}
	}

	return strings.Join(whereClause, " AND "), nil
}

func (s *Storage) GetICOs(page, perPage int, sort string, search string, filterByType string, baseCurrency string) (*ShortICOs, error) {
	var validSortParams = []string{
		"name",
		"start_date",
		"end_date",
		"funds_raised",
		"start_price",
		"soft_funds_cap",
		"hard_funds_cap",
		"progress",
		"title",
	}

	var (
		id, totalRows        int
		name, title, icoType string
		fundsRaised, startPrice, softFundsCap,
		hardFundsCap, progress NullFloat64
		//		tokenID            NullInt64
		startDate, endDate NullTime
	)

	safeSortParam, err := validateSortParam(validSortParams, sort)
	if err != nil {
		return nil, err
	}

	baseCurrency, err = validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}

	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}

	whereClause, err := getICOWhere(search, filterByType, time.Now())
	if err != nil {
		return nil, err
	}

	q := fmt.Sprintf(`
    SELECT
        icos.id,
        name,
        tokens.title,
        %s,
        %s AS progress,
        start_date,
        end_date,
        (funds_raised / %[3]f),
        (start_price / %[3]f),
        (soft_funds_cap / %[3]f),
        (hard_funds_cap / %[3]f),
        COUNT(*) OVER() AS total_rows
    FROM icos
    INNER JOIN tokens ON tokens.id = icos.token_id
    WHERE %s
    ORDER BY %s %s NULLS LAST
    LIMIT $1
    OFFSET $2
  `, TYPE_FIELD_DEFINITION, PROGRESS_FIELD_DEFINITION, basePrice, whereClause, safeSortParam.Name, safeSortParam.Direction)

	limit := perPage
	offset := page*perPage - perPage

	icoRows, err := s.db.Query(q, limit, offset)

	if err != nil {
		return nil, handleSQLError(err, "ICO not found")
	}

	defer icoRows.Close()

	var list = []ShortICO{}
	var ids = []string{}
	for i := 0; icoRows.Next(); i++ {
		icoRows.Scan(
			&id,
			&name,
			&title,
			&icoType,
			&progress,
			&startDate,
			&endDate,
			&fundsRaised,
			&startPrice,
			&softFundsCap,
			&hardFundsCap,
			&totalRows,
		)
		st := ShortICO{
			id,
			name,
			title,
			icoType,
			progress,
			startDate,
			endDate,
			fundsRaised,
			startPrice,
			softFundsCap,
			hardFundsCap,
			nil,
		}
		// TODO Check if we should specify slice length or capacity for performance reasons.
		list = append(list, st)
		ids = append(ids, strconv.Itoa(id))
	}

	// requests ico links
	linksMap, err := s.getLinksFor("ico", ids)
	if err != nil {
		return nil, handleSQLError(err, "Can't retrieve ico links")
	}

	// assign links for icos
	for idx, item := range list {
		icoLinks, ok := linksMap[item.ID]
		if ok {
			list[idx].Links = icoLinks
		} else {
			list[idx].Links = []Link{}
		}
	}

	meta := ICOMeta{page, perPage, totalRows, sort, search, baseCurrency}
	return &ShortICOs{meta, list}, nil
}

func (s *Storage) GetICOByName(icoName string, baseCurrency string) (*ICODetails, error) {
	var (
		err error
		id  int
		name, description, whitepaper,
		linkType, linkTitle, linkURL,
		icoType, title string
		startDate, endDate NullTime
		fundsRaised, softFundsCap,
		hardFundsCap, startPrice, progress NullFloat64
	)

	baseCurrency, err = validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}

	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}

	// Fetch information about ico.
	icoQuery := fmt.Sprintf(`
        SELECT
        icos.id,
        name,
        tokens.title,
        %s,
        %s,
        start_date,
        end_date,
        (funds_raised / %[3]f),
        (start_price / %[3]f),
        (soft_funds_cap / %[3]f),
        (hard_funds_cap / %[3]f),
        icos.description,
        whitepaper
    FROM icos
    INNER JOIN tokens ON tokens.id = icos.token_id
    WHERE name = $1`, TYPE_FIELD_DEFINITION, PROGRESS_FIELD_DEFINITION, basePrice)

	err = s.db.QueryRow(icoQuery, icoName).Scan(
		&id,
		&name,
		&title,
		&icoType,
		&progress,
		&startDate,
		&endDate,
		&fundsRaised,
		&startPrice,
		&softFundsCap,
		&hardFundsCap,
		&description,
		&whitepaper,
	)

	// If there is no ico - do not need to do additional queries
	if err != nil {
		return nil, handleSQLError(err, "ICO not found")
	}

	// Request links
	linksQuery := `
    SELECT
      type,
      title,
      url
    FROM ico_links
    WHERE ico_id = $1`

	icoLinkRows, err2 := s.db.Query(linksQuery, id)

	var links = make([]Link, 0)
	if err2 == nil {
		for i := 0; icoLinkRows.Next(); i++ {
			icoLinkRows.Scan(
				&linkType,
				&linkTitle,
				&linkURL,
			)
			link := Link{linkType, linkTitle, linkURL}
			links = append(links, link)
		}
		defer icoLinkRows.Close()
	}

	ico := ICO{
		id,
		name,
		title,
		icoType,
		progress,
		startDate,
		endDate,
		fundsRaised,
		startPrice,
		softFundsCap,
		hardFundsCap,
		description,
		whitepaper,
		links,
	}
	// TODO handle base_currency
	return &ICODetails{DetailsMeta{baseCurrency}, ico}, nil
}

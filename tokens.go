package main

import (
	"fmt"
	"strconv"
	"time"
)

// ExchangeDataTTL shows how long token exchange data is considered to be valid and can
// be used in token price/volume calculations.
const ExchangeDataTTL = time.Duration(24 * time.Hour)

type TokenMarket struct {
	ExchangeName  string      `json:"exchange_name"`
	ExchangeTitle string      `json:"exchange_title"`
	Price         float64     `json:"price"`
	Volume24h     NullFloat64 `json:"volume_24h"`
	UpdatedAt     time.Time   `json:"updated_at"`
}

type ShortToken struct {
	// Id field needs to assign links to token, we do not need to return it
	Id                int         `json:"-"`
	Symbol            string      `json:"symbol"`
	Title             string      `json:"title"`
	ImageSmall        NullString  `json:"image_small"`
	CirculatingSupply NullFloat64 `json:"circulating_supply"`
	TotalSupply       NullFloat64 `json:"total_supply"`
	Price             float32     `json:"price"`
	Volume24h         NullFloat64 `json:"volume_24h"`
	MarketCap         NullFloat64 `json:"market_cap"`
	Change1h          NullFloat64 `json:"change_1h"`
	Change24h         NullFloat64 `json:"change_24h"`
	Change7d          NullFloat64 `json:"change_7d"`
	Change1y          NullFloat64 `json:"change_1y"`
	Links             []Link      `json:"links"`
}

type ShortTokens struct {
	Meta        Meta         `json:"meta"`
	ShortTokens []ShortToken `json:"data"`
}

type Token struct {
	Title             string        `json:"title"`
	Symbol            string        `json:"symbol"`
	Description       string        `json:"description"`
	Image             NullString    `json:"image"`
	ImageSmall        NullString    `json:"image_small"`
	MetaKeywords      string        `json:"meta_keywords"`
	MetaDescription   string        `json:"meta_description"`
	CirculatingSupply NullFloat64   `json:"circulating_supply"`
	TotalSupply       NullFloat64   `json:"total_supply"`
	Price             NullFloat64   `json:"price"`
	Volume24h         NullFloat64   `json:"volume_24h"`
	MarketCap         NullFloat64   `json:"market_cap"`
	Change1h          NullFloat64   `json:"change_1h"`
	Change24h         NullFloat64   `json:"change_24h"`
	Change7d          NullFloat64   `json:"change_7d"`
	Change1y          NullFloat64   `json:"change_1y"`
	High24h           NullFloat64   `json:"high_24h"`
	Low24h            NullFloat64   `json:"low_24h"`
	High1y            NullFloat64   `json:"high_1y"`
	Low1y             NullFloat64   `json:"low_1y"`
	AvgDailyVolume    NullFloat64   `json:"avg_daily_volume"`
	Links             []Link        `json:"links"`
	Markets           []TokenMarket `json:"markets"`
}

type TokenDetails struct {
	DetailsMeta DetailsMeta `json:"meta"`
	Token       Token       `json:"data"`
}

func (s *Storage) GetTokens(page, perPage int, sort string, search string, baseCurrency string) (*ShortTokens, error) {
	var validSortParams = []string{
		"symbol",
		"title",
		"price",
		"volume_24h",
		"circulating_supply",
		"total_supply",
		"market_cap",
		"change_1h",
		"change_24h",
		"change_7d",
		"change_1y",
	}

	var (
		id            int
		symbol, title string
		price         float32
		circulatingSupply, totalSupply, volume24h,
		marketCap, change1h, change24h, change7d, change1y NullFloat64
		imageSmall NullString
		totalRows  int
	)

	baseCurrency, err := validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}

	safeSortParam, err := validateSortParam(validSortParams, sort)
	if err != nil {
		return nil, err
	}

	whereClause := "active=True" // Dummy where clause
	if search != "" {
		whereClause += fmt.Sprintf(` AND (symbol ILIKE '%%%s%%' OR title ILIKE '%%%s%%')`, search, search)
	}

	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}
	// TODO fields 'change_XX' are depends on baseCurrency as well.
	// We need to substitute them somehow after fetchkin will write changes in
	// different currencies.
	marketCapFieldDefinition := fmt.Sprintf(`
		CASE WHEN circulating_supply IS NOT NULL
			THEN (tokens.circulating_supply * AVG(data.price / %[1]f))
			ELSE (tokens.total_supply * AVG(data.price / %[1]f))
		END
	`, basePrice)

	priceFields := fmt.Sprintf(`
		tokens.change_1h,
		tokens.change_24h,
		tokens.change_7d,
		tokens.change_1y,
		(AVG(data.price) / %[1]f) AS price,
		(SUM(data.volume_24h / %[1]f)) AS volume_24h,
		%s AS market_cap,
	`, basePrice, marketCapFieldDefinition)
	// We are using inner join for now to return only tokens with defined price.
	// We might want to change this logic in the future and return all tokens even
	// without prices. In that case we should use left join and handle NULL columns.

	dataValidFrom := s.exchangeDataValidFromDate()
	q := fmt.Sprintf(`
    SELECT
      tokens.id,
      tokens.symbol,
      tokens.title,
      tokens.image_small,
      tokens.circulating_supply,
      tokens.total_supply,
      %s
      COUNT(*) OVER() AS total_rows
    FROM tokens
      INNER JOIN token_exchange_data AS data
        ON data.token_id = tokens.id
        AND data.updated_at > '%s'
    WHERE %s
    GROUP BY tokens.id
    ORDER BY %s %s NULLS LAST, id DESC
    LIMIT $1
    OFFSET $2
  `, priceFields, dataValidFrom, whereClause, safeSortParam.Name, safeSortParam.Direction)

	limit := perPage
	offset := page*perPage - perPage

	tokenRows, err := s.db.Query(q, limit, offset)

	if err != nil {
		return nil, handleSQLError(err, "Tokens not found")
	}

	defer tokenRows.Close()

	var list = []ShortToken{}
	var ids = []string{}
	for i := 0; tokenRows.Next(); i++ {
		tokenRows.Scan(
			&id,
			&symbol,
			&title,
			&imageSmall,
			&circulatingSupply,
			&totalSupply,
			&change1h,
			&change24h,
			&change7d,
			&change1y,
			&price,
			&volume24h,
			&marketCap,
			&totalRows,
		)
		// TODO: find out which corner case is being handled by this clause
		if symbol == "" {
			list = make([]ShortToken, 0)
			break
		}
		imageSmall = s.normalizeImage(imageSmall)
		st := ShortToken{
			id,
			symbol,
			title,
			imageSmall,
			circulatingSupply,
			totalSupply,
			price,
			volume24h,
			marketCap,
			change1h,
			change24h,
			change7d,
			change1y,
			nil,
		}
		// TODO Check if we should specify slice length or capacity for performance reasons.
		list = append(list, st)
		ids = append(ids, strconv.Itoa(id))
	}

	// requests token links
	linksMap, err := s.getLinksFor("token", ids)
	if err != nil {
		return nil, handleSQLError(err, "Can't retrieve token links")
	}

	// assign links for tokens
	for idx, item := range list {
		tokenLinks, ok := linksMap[item.Id]
		if ok {
			list[idx].Links = tokenLinks
		} else {
			list[idx].Links = []Link{}
		}
	}

	meta := Meta{page, perPage, totalRows, sort, search, baseCurrency}
	return &ShortTokens{meta, list}, nil
}

func (s *Storage) GetTokenBySymbol(sym string, baseCurrency string) (*TokenDetails, error) {
	var (
		err                                       error
		id                                        int
		symbol, title, description                string
		image, imageSmall                         NullString
		circulatingSupply, totalSupply, volume24h NullFloat64
		marketCap                                 NullFloat64
		linkType, linkTitle, linkURL,
		exchangeName, exchangeTitle string
		metaKeywords, metaDescription string
		exchangeUpdatedAt             time.Time
		exchangeVolume24h, change1h, change24h, change7d, change1y,
		high24h, low24h, high1y, low1y, avgDailyVolume NullFloat64
		exchangePrice float64
		price         NullFloat64
	)

	baseCurrency, err = validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}
	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}

	dataValidFrom := s.exchangeDataValidFromDate()

	// Fetch information about token.
	tokensQuery := fmt.Sprintf(`
    SELECT
      tokens.id,
      tokens.symbol,
      tokens.title,
      tokens.description,
      tokens.image,
	  tokens.image_small,
	  tokens.meta_keywords,
	  tokens.meta_description,
      tokens.circulating_supply,
      tokens.total_supply,
      tokens.change_1h,
      tokens.change_24h,
      tokens.change_7d,
      tokens.change_1y,
      (tokens.high_24h / %[1]f) AS high_24h,
      (tokens.low_24h / %[1]f) AS low_24h,
      (tokens.high_1y / %[1]f) AS high_1y,
      (tokens.low_1y / %[1]f) AS low_1y,
      (tokens.avg_daily_volume / %[1]f) AS avg_daily_volume,
      AVG(data.price) / %[1]f AS price,
      SUM(data.volume_24h) / %[1]f AS volume_24h,
      (AVG(data.price) * tokens.circulating_supply) / %[1]f as market_cap
    FROM tokens
      LEFT JOIN token_exchange_data AS data
        ON data.token_id = tokens.id
        AND data.updated_at > '%s'
    WHERE tokens.symbol = $1
    GROUP BY tokens.id`, basePrice, dataValidFrom)

	err = s.db.QueryRow(tokensQuery, sym).Scan(
		&id,
		&symbol,
		&title,
		&description,
		&image,
		&imageSmall,
		&metaKeywords,
		&metaDescription,
		&circulatingSupply,
		&totalSupply,
		&change1h,
		&change24h,
		&change7d,
		&change1y,
		&high24h,
		&low24h,
		&high1y,
		&low1y,
		&avgDailyVolume,
		&price,
		&volume24h,
		&marketCap,
	)

	image = s.normalizeImage(image)
	imageSmall = s.normalizeImage(imageSmall)
	// If there is no token - do not need to do additional queries
	if err != nil {
		return nil, handleSQLError(err, "Token not found")
	}

	// Request links
	linksQuery := `
    SELECT
      type,
      title,
      url
    FROM token_links
    WHERE token_id = $1`

	tokenLinkRows, err2 := s.db.Query(linksQuery, id)

	var links = make([]Link, 0)
	if err2 == nil {
		// TODO maybe use for rows.Next() {} loop
		for i := 0; tokenLinkRows.Next(); i++ {
			tokenLinkRows.Scan(
				&linkType,
				&linkTitle,
				&linkURL,
			)
			link := Link{linkType, linkTitle, linkURL}
			links = append(links, link)
		}
		defer tokenLinkRows.Close()
	}

	// Request markets
	marketsQuery := fmt.Sprintf(`
    SELECT
      exchanges.name AS exchange_name,
      exchanges.title AS exchange_title,
      AVG(data.price) / %[1]f AS price,
      SUM(data.volume_24h) AS volume_24h,
      MAX(data.updated_at) AS updated_at
    FROM token_exchange_data AS data
      INNER JOIN exchanges
        ON data.exchange_id = exchanges.id
	WHERE data.token_id = $1 AND data.updated_at > '%s'
	GROUP BY exchanges.id
	ORDER BY volume_24h DESC`, basePrice, dataValidFrom)

	tokenMarketRows, err3 := s.db.Query(marketsQuery, id)

	var markets = make([]TokenMarket, 0)
	if err3 == nil {
		for i := 0; tokenMarketRows.Next(); i++ {
			tokenMarketRows.Scan(
				&exchangeName,
				&exchangeTitle,
				&exchangePrice,
				&exchangeVolume24h,
				&exchangeUpdatedAt,
			)
			market := TokenMarket{
				exchangeName,
				exchangeTitle,
				exchangePrice,
				exchangeVolume24h,
				exchangeUpdatedAt,
			}
			markets = append(markets, market)
		}
		defer tokenMarketRows.Close()
	}

	// TODO check different corner cases and correponding errors
	token := Token{
		title, symbol, description, image, imageSmall, metaKeywords, metaDescription,
		circulatingSupply, totalSupply, price,
		volume24h, marketCap, change1h, change24h, change7d, change1y,
		high24h, low24h, high1y, low1y, avgDailyVolume, links, markets}

	return &TokenDetails{DetailsMeta{baseCurrency}, token}, nil
}

// This method returns the date (in ISO format) after token exchange data is considered to be valid.
// It is used during token price/volume calculations. Valid period is adjustable by setting the
// `ExchangeDataTTL` const value.
func (s *Storage) exchangeDataValidFromDate() string {
	return time.Now().
		Add(-1 * ExchangeDataTTL).
		UTC().
		Format(time.RFC3339)
}

func (s *Storage) getBasePriceFor(baseCurrency string) (float64, error) {
	var price float64

	if baseCurrency == "usd" {
		return 1, nil
	}

	cachePrice, err := s.cache.Get([]byte(fmt.Sprintf("base-price-%s", baseCurrency)))
	if err != nil {
		return s.getBasePriceFromDB(baseCurrency)
	}
	price, err = strconv.ParseFloat(string(cachePrice), 64)

	if err != nil {
		return s.getBasePriceFromDB(baseCurrency)
	}

	return price, nil
}

func (s *Storage) getBasePriceFromDB(baseCurrency string) (float64, error) {
	var price float64

	dataValidFrom := s.exchangeDataValidFromDate()
	q := `
		SELECT
			AVG(data.price) AS price
		FROM tokens
		INNER JOIN token_exchange_data AS data
			ON data.token_id = tokens.id
		WHERE tokens.symbol = $1 AND data.updated_at > $2
		GROUP BY tokens.id
	  `
	err := s.db.QueryRow(q, baseCurrency, dataValidFrom).Scan(&price)
	if err != nil {
		return 0, err
	}
	key := []byte(fmt.Sprintf("base-price-%s", baseCurrency))
	value := []byte(strconv.FormatFloat(price, 'E', -1, 64))
	s.cache.Set(key, value, 120)
	return price, nil
}

func (s *Storage) normalizeImage(img NullString) NullString {
	if img.Valid {
		img.String = s.config.Images.BaseURL + img.String
	}
	return img
}

package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/ant0ine/go-json-rest/rest"
	"github.com/ant0ine/go-json-rest/rest/test"
)

type ResultError struct {
	Error string `json:"Error"`
}

func RunService(storage StorageInterface) *rest.Api {
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)

	router, err := rest.MakeRouter(GetRoutes(&Ctx{storage})...)
	if err != nil {
		log.Fatal(err)
	}

	api.SetApp(router)
	return api
}

// Mock 404 responses.
// Implements StorageInterface.
type Mock404Storage struct{}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (ms *Mock404Storage) CheckDBStatus() (*Status, error) {
	return &Status{"ok"}, nil
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (ms *Mock404Storage) GetTokens(page, perPage int, sort string, search string, baseCurrency string) (*ShortTokens, error) {
	return nil, errors.New("Not implemented")
}

func (ms *Mock404Storage) GetTokenBySymbol(sym string, baseCurrency string) (*TokenDetails, error) {
	return nil, &DataNotFoundError{sql.ErrNoRows, "Token not found"}
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (ms *Mock404Storage) GetExchanges(page, perPage int, sort string, search string, baseCurrency string) (*Exchanges, error) {
	return nil, errors.New("Not implemented")
}

func (ms *Mock404Storage) GetExchangeByName(name string, baseCurrency string) (*ExchangeDetails, error) {
	return nil, &DataNotFoundError{sql.ErrNoRows, "Exchange not found"}
}

func (ms *Mock404Storage) GetExchangePairs(name string, sort string, search string, baseCurrency string) (*ExchangePairs, error) {
	return nil, &DataNotFoundError{sql.ErrNoRows, "Pairs not found"}
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (s *Mock404Storage) GetICOs(page, perPage int, sort string, search string, filterByType string, baseCurrency string) (*ShortICOs, error) {
	return nil, errors.New("Not implemented")
}

func (s *Mock404Storage) GetICOByName(icoName string, baseCurrency string) (*ICODetails, error) {
	return nil, &DataNotFoundError{sql.ErrNoRows, "ICO not found"}
}

// Test 404 responses.
func Test404Requests(t *testing.T) {
	storage := &Mock404Storage{}
	api := RunService(storage)

	for url, reason := range map[string]string{
		"http://1.2.3.4/v1/tokens/test":          "Token not found",
		"http://1.2.3.4/v1/exchanges/test":       "Exchange not found",
		"http://1.2.3.4/v1/exchanges/test/pairs": "Pairs not found",
	} {
		request := test.MakeSimpleRequest("GET", url, nil)

		recorded := test.RunRequest(t, api.MakeHandler(), request)
		recorded.CodeIs(404)
		recorded.ContentTypeIsJson()

		JSONAssert(recorded, fmt.Sprintf(`{"Error":"%s"}`, reason))
	}
}

// Mock 400 responses.
// Implements StorageInterface.
type Mock400Storage struct{}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (ms *Mock400Storage) CheckDBStatus() (*Status, error) {
	return &Status{"ok"}, nil
}

func (ms *Mock400Storage) GetTokens(page, perPage int, sort string, search string, baseCurrency string) (*ShortTokens, error) {
	return nil, &BadRequestError{fmt.Sprintf("Invalid sort param value: %s", sort)}
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (ms *Mock400Storage) GetTokenBySymbol(sym string, baseCurrency string) (*TokenDetails, error) {
	return nil, errors.New("Not implemented")
}

func (ms *Mock400Storage) GetExchanges(page, perPage int, sort string, search string, baseCurrency string) (*Exchanges, error) {
	return nil, &BadRequestError{fmt.Sprintf("Invalid sort param value: %s", sort)}
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (ms *Mock400Storage) GetExchangeByName(name string, baseCurrency string) (*ExchangeDetails, error) {
	return nil, errors.New("Not implemented")
}

func (ms *Mock400Storage) GetExchangePairs(name string, sort string, search string, baseCurrency string) (*ExchangePairs, error) {
	return nil, &DataNotFoundError{sql.ErrNoRows, "Not implemented"}
}

func (s *Mock400Storage) GetICOs(page, perPage int, sort string, search string, filterByType string, baseCurrency string) (*ShortICOs, error) {
	return nil, &BadRequestError{fmt.Sprintf("Invalid sort param value: %s", sort)}
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (s *Mock400Storage) GetICOByName(icoName string, baseCurrency string) (*ICODetails, error) {
	return nil, errors.New("Not implemented")
}

// Test 400 responses. We return 400 if sort param is invalid.
func Test400Requests(t *testing.T) {
	storage := &Mock400Storage{}
	api := RunService(storage)

	for url, reason := range map[string]string{
		"http://1.2.3.4/v1/tokens?sort=test":     "Invalid sort param value: test",
		"http://1.2.3.4/v1/tokens?sort=-test":    "Invalid sort param value: -test",
		"http://1.2.3.4/v1/exchanges?sort=test":  "Invalid sort param value: test",
		"http://1.2.3.4/v1/exchanges?sort=-test": "Invalid sort param value: -test",
	} {
		request := test.MakeSimpleRequest("GET", url, nil)

		recorded := test.RunRequest(t, api.MakeHandler(), request)
		recorded.CodeIs(400)
		recorded.ContentTypeIsJson()

		JSONAssert(recorded, fmt.Sprintf(`{"Error":"%s"}`, reason))
	}
}

// Mock 200 responses. Happy path.
// Implements StorageInterface.
type MockStorage struct{}

func (ms *MockStorage) CheckDBStatus() (*Status, error) {
	return &Status{"ok"}, nil
}

func (ms *MockStorage) GetTokens(page, perPage int, sort string, search string, baseCurrency string) (*ShortTokens, error) {
	baseCurrency, err := validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}
	meta := Meta{page, perPage, 100, sort, search, baseCurrency}
	var list []ShortToken
	// Simulate empty response
	if search == "NOT_FOUND" {
		list = []ShortToken{}
	} else {
		list = []ShortToken{
			ShortToken{1,
				"btc",
				"Bitcoin",
				NullString{Valid: true, String: "https://localhost/btc_small.png"},
				NullFloat64{Valid: true, Float64: 10.01},
				NullFloat64{Valid: true, Float64: 10.02},
				12773.249,
				NullFloat64{Valid: false, Float64: 0.0},
				NullFloat64{Valid: true, Float64: 100.1},
				NullFloat64{Valid: false, Float64: 0.1},
				NullFloat64{Valid: false, Float64: 0.2},
				NullFloat64{Valid: false, Float64: 0.3},
				NullFloat64{Valid: false, Float64: 0.4},
				[]Link{{"twitter", "Twitter", "https://twitter.com/test"}},
			},
		}
	}
	return &ShortTokens{meta, list}, nil
}

func (ms *MockStorage) GetTokenBySymbol(sym string, baseCurrency string) (*TokenDetails, error) {
	links := []Link{
		Link{"twitter", "Bitcoin", "https://twitter.com/bitcoin"},
	}
	updatedAt := time.Date(
		1, 1, 1, 0, 0, 0, 0, time.UTC,
	)
	markets := []TokenMarket{
		TokenMarket{"bitfinex", "Bitfinex", 12773.249, NullFloat64{Valid: true, Float64: 01.10}, updatedAt},
	}
	token := Token{
		"Bitcoin",
		"btc",
		"Bitcoin Description",
		NullString{Valid: true, String: "https://localhost/btc.png"},
		NullString{Valid: true, String: "https://localhost/btc_small.png"},
		"keyword1,keyword2",
		"Meta description",
		NullFloat64{Valid: true, Float64: 10.01},
		NullFloat64{Valid: true, Float64: 10.02},
		NullFloat64{Valid: true, Float64: 12773.249},
		NullFloat64{Valid: false, Float64: 0.0},
		NullFloat64{Valid: true, Float64: 100.2},
		NullFloat64{Valid: false, Float64: 0.1},
		NullFloat64{Valid: false, Float64: 0.2},
		NullFloat64{Valid: false, Float64: 0.3},
		NullFloat64{Valid: false, Float64: 0.4},
		NullFloat64{Valid: true, Float64: 0.4},
		NullFloat64{Valid: true, Float64: 0.2},
		NullFloat64{Valid: true, Float64: 1.4},
		NullFloat64{Valid: true, Float64: 0.1},
		NullFloat64{Valid: true, Float64: 100.10},
		links,
		markets,
	}
	return &TokenDetails{DetailsMeta{baseCurrency}, token}, nil
}

func (ms *MockStorage) GetExchanges(page, perPage int, sort string, search string, baseCurrency string) (*Exchanges, error) {
	if baseCurrency == "" {
		baseCurrency = "usd"
	}
	updatedAt := time.Date(
		1, 1, 1, 0, 0, 0, 0, time.UTC,
	)
	meta := Meta{page, perPage, 100, sort, search, baseCurrency}
	var list []ShortExchange
	if search == "NOT_FOUND" {
		list = []ShortExchange{}
	} else {
		list = []ShortExchange{
			ShortExchange{
				1,
				"bitfinex",
				"Bitfinex",
				"United States",
				NullFloat64{Valid: true, Float64: 0.1},
				[]Link{{"twitter", "Twitter account", "http://twitter.com/bitfinex"}},
				[]ExchangePair{{"btc", "eth", 0.2, NullFloat64{Valid: true, Float64: 0.4}, updatedAt}},
			},
		}
	}
	return &Exchanges{meta, list}, nil
}

func (ms *MockStorage) GetExchangeByName(name string, baseCurrency string) (*ExchangeDetails, error) {
	if baseCurrency == "" {
		baseCurrency = "usd"
	}
	links := []Link{
		Link{"twitter", "bitfinex", "https://twitter.com/bitfinex"},
	}
	exchange := Exchange{
		"bitfinex",
		"Bitfinex",
		"Russia",
		NullFloat64{Valid: true, Float64: 0.2},
		links,
		[]ExchangePair{
			{"btc", "eth", 0.1, NullFloat64{Valid: false, Float64: 10.5}, time.Date(
				1, 1, 1, 0, 0, 0, 0, time.UTC,
			)},
		},
	}
	return &ExchangeDetails{DetailsMeta{baseCurrency}, exchange}, nil
}

func (ms *MockStorage) GetExchangePairs(name string, sort string, search string, baseCurrency string) (*ExchangePairs, error) {
	if baseCurrency == "" {
		baseCurrency = "usd"
	}
	updatedAt := time.Date(
		1, 1, 1, 0, 0, 0, 0, time.UTC,
	)
	meta := PairsMeta{sort, search, baseCurrency}
	var list []DetailedExchangePair
	if search == "NOT_FOUND" {
		list = []DetailedExchangePair{}
	} else {
		list = []DetailedExchangePair{
			DetailedExchangePair{
				"Bitcoin",
				"btc",
				"btc",
				"usd",
				100,
				NullFloat64{Valid: true, Float64: 200},
				updatedAt,
			},
		}
	}
	return &ExchangePairs{meta, list}, nil
}

// We do not test this method, but it should be implemented
// to comply with the StorageInterface
func (s *MockStorage) GetICOs(page, perPage int, sort string, search string, filterByType string, baseCurrency string) (*ShortICOs, error) {
	if baseCurrency == "" {
		baseCurrency = "usd"
	}
	meta := ICOMeta{page, perPage, 100, sort, search, baseCurrency}
	var list []ShortICO
	list = []ShortICO{
		ShortICO{
			1,
			"eth",
			"Ethereum",
			"upcoming",
			NullFloat64{Valid: true, Float64: 50},
			NullTime{Valid: true, Time: time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC)},
			NullTime{Valid: true, Time: time.Date(2, 2, 2, 0, 0, 0, 0, time.UTC)},
			NullFloat64{Valid: true, Float64: 100},
			NullFloat64{Valid: true, Float64: 100.00},
			NullFloat64{Valid: true, Float64: 10.00},
			NullFloat64{Valid: true, Float64: 3000.0},
			[]Link{{"twitter", "Twitter account", "http://twitter.com/eth"}},
		},
	}
	return &ShortICOs{meta, list}, nil
}

func (s *MockStorage) GetICOByName(icoName string, baseCurrency string) (*ICODetails, error) {
	if baseCurrency == "" {
		baseCurrency = "usd"
	}
	ico := ICO{
		1,
		"eth",
		"Ethereum",
		"finished",
		NullFloat64{Valid: false, Float64: 0},
		NullTime{Valid: true, Time: time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC)},
		NullTime{Valid: true, Time: time.Date(2, 2, 2, 0, 0, 0, 0, time.UTC)},
		NullFloat64{Valid: true, Float64: 100.00},
		NullFloat64{Valid: true, Float64: 10.00},
		NullFloat64{Valid: true, Float64: 2000},
		NullFloat64{Valid: true, Float64: 3000},
		"ICO Description",
		"https://example.com/whitepaper.pdf",
		[]Link{{"twitter", "Twitter account", "http://twitter.com/eth"}},
	}
	return &ICODetails{DetailsMeta{baseCurrency}, ico}, nil
}

func compactJsonString(jsonString []byte) (string, error) {
	compactBuffer := new(bytes.Buffer)
	err := json.Compact(compactBuffer, jsonString)
	if err != nil {
		return "", err
	}
	return compactBuffer.String(), nil
}

func JSONAssert(rec *test.Recorded, jsonString string) {
	body, err := rec.DecodedBody()

	if err != nil {
		rec.T.Error("\n...Can't decode body")
		return
	}
	compactBody, err := compactJsonString(body)
	if err != nil {
		rec.T.Errorf("Can't compact json body: %s", body)
	}
	compactJson, err := compactJsonString([]byte(jsonString))
	if err != nil {
		rec.T.Errorf("Can't compact exepct json string: %s", jsonString)
	}
	if compactBody != compactJson {
		rec.T.Errorf("\n...expected: %v\n...got: %v", compactJson, compactBody)
	}
}

func Test200Requests(t *testing.T) {
	storage := &MockStorage{}
	api := RunService(storage)

	assertions := map[string]func(*test.Recorded){
		"http://1.2.3.4/v1/status": func(rec *test.Recorded) {
			// TODO rewrite other tests using the same approach
			JSONAssert(rec, `{"status":"ok"}`)
		},
		// Default params values are: page=1, per_page=10, sort=symbol
		"http://1.2.3.4/v1/tokens": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "symbol",
					"search": "",
					"base_currency": "usd"
				},
				"data": [
					{
						"symbol": "btc",
						"title": "Bitcoin",
						"image_small": "https://localhost/btc_small.png",
						"circulating_supply": 10.01,
						"total_supply": 10.02,
						"price": 12773.249,
						"volume_24h": null,
						"market_cap": 100.1,
						"change_1h":null,
						"change_24h":null,
						"change_7d":null,
						"change_1y":null,
						"links":[{
							"type":"twitter",
							"title":"Twitter",
							"url":"https://twitter.com/test"
						}]
					}
				]
			}`)
		},
		"http://1.2.3.4/v1/tokens?search=searchString": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "symbol",
					"search": "searchString",
					"base_currency": "usd"
				},
				"data": [
					{
						"symbol": "btc",
						"title": "Bitcoin",
						"image_small": "https://localhost/btc_small.png",
						"circulating_supply": 10.01,
						"total_supply": 10.02,
						"price": 12773.249,
						"volume_24h": null,
						"market_cap": 100.1,
						"change_1h":null,
						"change_24h":null,
						"change_7d":null,
						"change_1y":null,
						"links":[{
							"type":"twitter",
							"title":"Twitter",
							"url":"https://twitter.com/test"
						}]
					}
				]
			}`)
		},
		"http://1.2.3.4/v1/tokens?search=NOT_FOUND": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "symbol",
					"search": "NOT_FOUND",
					"base_currency": "usd"
				},
				"data": []
			}`)
		},
		"http://1.2.3.4/v1/tokens?search=NOT_FOUND&base_currency=eth": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "symbol",
					"search": "NOT_FOUND",
					"base_currency": "eth"
				},
				"data": []
			}`)
		},
		"http://1.2.3.4/v1/tokens/btc": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"base_currency": "usd"
				},
				"data": {
					"title": "Bitcoin",
					"symbol": "btc",
					"description": "Bitcoin Description",
					"image": "https://localhost/btc.png",
					"image_small": "https://localhost/btc_small.png",
					"meta_keywords": "keyword1,keyword2",
					"meta_description": "Meta description",
					"circulating_supply": 10.01,
					"total_supply": 10.02,
					"price": 12773.249,
					"volume_24h": null,
					"market_cap": 100.2,
					"change_1h":null,
					"change_24h":null,
					"change_7d":null,
					"change_1y":null,
					"high_24h": 0.4,
					"low_24h": 0.2,
					"high_1y": 1.4,
					"low_1y": 0.1,
					"avg_daily_volume": 100.1,
					"links": [
						{
							"type": "twitter",
							"title": "Bitcoin",
							"url": "https://twitter.com/bitcoin"
						}
					],
					"markets": [
						{
							"exchange_name": "bitfinex",
							"exchange_title": "Bitfinex",
							"price": 12773.249,
							"volume_24h": 1.1,
							"updated_at": "0001-01-01T00:00:00Z"
						}
					]
				}
			}`)
		},
		// Default params values are: page=1, per_page=10, sort=name
		"http://1.2.3.4/v1/exchanges": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "name",
					"search": "",
					"base_currency": "usd"
				},
				"data": [
					{
						"name": "bitfinex",
						"title": "Bitfinex",
						"country": "United States",
						"volume_24h": 0.1,
						"links": [{
							"type": "twitter",
							"title": "Twitter account",
							"url": "http://twitter.com/bitfinex"
						}],
						"pairs": [{
							"from_symbol":"btc",
							"to_symbol":"eth",
							"price":0.2,
							"volume_24h":0.4,
							"updated_at":"0001-01-01T00:00:00Z"
						}]
					}
				]
			}`)
		},
		"http://1.2.3.4/v1/exchanges?search=searchString": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "name",
					"search": "searchString",
					"base_currency": "usd"
				},
				"data": [
					{
						"name": "bitfinex",
						"title": "Bitfinex",
						"country": "United States",
						"volume_24h": 0.1,
						"links": [{
							"type": "twitter",
							"title": "Twitter account",
							"url": "http://twitter.com/bitfinex"
						}],
						"pairs": [{
							"from_symbol":"btc",
							"to_symbol":"eth",
							"price":0.2,
							"volume_24h":0.4,
							"updated_at":"0001-01-01T00:00:00Z"
						}]
					}
				]
			}`)
		},
		"http://1.2.3.4/v1/exchanges?search=NOT_FOUND": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"page": 1,
					"per_page": 10,
					"count": 100,
					"sort": "name",
					"search": "NOT_FOUND",
					"base_currency": "usd"
				},
				"data": []
			}`)
		},
		"http://1.2.3.4/v1/exchanges/bitfinex": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"base_currency": "usd"
				},
				"data": {
					"name": "bitfinex",
					"title": "Bitfinex",
					"country": "Russia",
					"volume_24h": 0.2,
					"links": [
						{
							"type": "twitter",
							"title": "bitfinex",
							"url": "https://twitter.com/bitfinex"
						}
					],
					"pairs": [{
						"from_symbol": "btc",
						"to_symbol": "eth",
						"price": 0.1,
						"volume_24h": null,
						"updated_at": "0001-01-01T00:00:00Z"
					}]
				}
			}`)
		},
		"http://1.2.3.4/v1/exchanges/bitfinex/pairs": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"sort": "-volume_24h",
					"search": "",
					"base_currency": "usd"
				},
				"data": [{
					"token_title": "Bitcoin",
					"token_symbol": "btc",
					"from_symbol": "btc",
					"to_symbol": "usd",
					"price": 100,
					"volume_24h": 200,
					"updated_at": "0001-01-01T00:00:00Z"
				}]
			}`)
		},
		"http://1.2.3.4/v1/icos": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta":{
				   "page":1,
				   "per_page":10,
				   "count":100,
				   "sort":"name",
				   "search":"",
				   "base_currency":"usd"
				},
				"data":[
				   {
					  "name":"eth",
					  "title":"Ethereum",
					  "type":"upcoming",
					  "progress": 50,
					  "start_date":"0001-01-01T00:00:00Z",
					  "end_date":"0002-02-02T00:00:00Z",
					  "funds_raised":100,
					  "start_price":100,
					  "soft_funds_cap":10,
					  "hard_funds_cap":3000,
					  "links":[
						 {
							"type":"twitter",
							"title":"Twitter account",
							"url":"http://twitter.com/eth"
						 }
					  ]
				   }
				]
			 }`)
		},
		"http://1.2.3.4/v1/icos/eth": func(rec *test.Recorded) {
			JSONAssert(rec, `{
				"meta": {
					"base_currency": "usd"
				},
				"data":{
				   "name":"eth",
				   "title":"Ethereum",
				   "type":"finished",
				   "progress":null,
				   "start_date":"0001-01-01T00:00:00Z",
				   "end_date":"0002-02-02T00:00:00Z",
				   "funds_raised":100,
				   "start_price":10,
				   "soft_funds_cap":2000,
				   "hard_funds_cap":3000,
				   "description":"ICO Description",
				   "whitepaper":"https://example.com/whitepaper.pdf",
				   "links":[
					  {
						 "type":"twitter",
						 "title":"Twitter account",
						 "url":"http://twitter.com/eth"
					  }
				   ]
				}
			 }`)
		},
	}

	for url, fn := range assertions {
		request := test.MakeSimpleRequest("GET", url, nil)
		recorded := test.RunRequest(t, api.MakeHandler(), request)
		recorded.CodeIs(200)
		recorded.ContentTypeIsJson()
		fn(recorded)
	}

}

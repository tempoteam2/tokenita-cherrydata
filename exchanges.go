package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// How many pairs will be returned for /exchanges endpoint
const EXCHANGE_PAIRS_COUNT = 3

type ShortExchange struct {
	Id        int            `json:"-"`
	Name      string         `json:"name"`
	Title     string         `json:"title"`
	Country   string         `json:"country"`
	Volume24h NullFloat64    `json:"volume_24h"`
	Links     []Link         `json:"links"`
	Pairs     []ExchangePair `json:"pairs"`
}

type DetailedExchangePair struct {
	TokenTitle  string      `json:"token_title"`
	TokenSymbol string      `json:"token_symbol"`
	FromSymbol  string      `json:"from_symbol"`
	ToSymbol    string      `json:"to_symbol"`
	Price       float64     `json:"price"`
	Volume24h   NullFloat64 `json:"volume_24h"`
	UpdatedAt   time.Time   `json:"updated_at"`
}

type ExchangePair struct {
	FromSymbol string      `json:"from_symbol"`
	ToSymbol   string      `json:"to_symbol"`
	Price      float64     `json:"price"`
	Volume24h  NullFloat64 `json:"volume_24h"`
	UpdatedAt  time.Time   `json:"updated_at"`
}

type Exchange struct {
	Name      string         `json:"name"`
	Title     string         `json:"title"`
	Country   string         `json:"country"`
	Volume24h NullFloat64    `json:"volume_24h"`
	Links     []Link         `json:"links"`
	Pairs     []ExchangePair `json:"pairs"`
}

type ExchangePairs struct {
	PairsMeta PairsMeta              `json:"meta"`
	Data      []DetailedExchangePair `json:"data"`
}

type Exchanges struct {
	Meta Meta            `json:"meta"`
	Data []ShortExchange `json:"data"`
}

type PairsMeta struct {
	Sort         string `json:"sort"`
	Search       string `json:"search"`
	BaseCurrency string `json:"base_currency"`
}

type ExchangeDetails struct {
	DetailsMeta DetailsMeta `json:"meta"`
	Data        Exchange    `json:"data"`
}

func (s *Storage) GetExchanges(page, perPage int, sort string, search string, baseCurrency string) (*Exchanges, error) {
	var (
		name, title, country string
		validSortParams      = []string{"name", "title", "volume_24h"}
		id, totalRows        int
		volume24h            NullFloat64
	)

	safeSortParam, err := validateSortParam(validSortParams, sort)
	if err != nil {
		return nil, err
	}

	baseCurrency, err = validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}

	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}

	whereClause := "True"
	if search != "" {
		whereClause = fmt.Sprintf(`(t.name ILIKE '%%%s%%' OR t.title ILIKE '%%%s%%')`, search, search)
	}
	dataValidFrom := s.exchangeDataValidFromDate()

	q := fmt.Sprintf(`
    SELECT
      t.id,
      t.name,
	  t.title,
	  t.country,
      (SUM(data.volume_24h) / %[1]f) AS volume_24h,
      COUNT(*) OVER() AS total_rows
    FROM exchanges as t
    INNER JOIN token_exchange_data as data
      ON data.exchange_id = t.id AND data.updated_at > $1
    WHERE %s
    GROUP BY t.id
    ORDER BY %s %s
    LIMIT $2
    OFFSET $3`, basePrice, whereClause, safeSortParam.Name, safeSortParam.Direction)

	limit := perPage
	offset := page*perPage - perPage

	exchangeRows, err := s.db.Query(q, dataValidFrom, limit, offset)
	if err != nil {
		return nil, handleSQLError(err, "Exchanges not found")
	}

	defer exchangeRows.Close()

	var list = []ShortExchange{}
	ids := []string{}
	for i := 0; exchangeRows.Next(); i++ {
		exchangeRows.Scan(
			&id,
			&name,
			&title,
			&country,
			&volume24h,
			&totalRows,
		)
		// TODO: find out which corner case is being handled by this clause
		if name == "" {
			list = make([]ShortExchange, 0)
			break
		}
		ex := ShortExchange{id, name, title, country, volume24h, nil, nil}
		list = append(list, ex)
		ids = append(ids, strconv.Itoa(id))
	}

	// requests token links
	linksMap, err := s.getLinksFor("exchange", ids)
	if err != nil {
		return nil, handleSQLError(err, "Can't retrieve exchanges links")
	}

	pairsMap, err := s.GetPairsForExchanges(ids, basePrice)
	if err != nil {
		return nil, handleSQLError(err, "Can't retrieve exchange pairs")
	}

	for idx, item := range list {
		// assign links for exchanges
		exchangeLinks, ok := linksMap[item.Id]
		if ok {
			list[idx].Links = exchangeLinks
		} else {
			list[idx].Links = []Link{}
		}

		// assign pairs for exchanges
		exchangePairs, ok := pairsMap[item.Id]
		if ok {
			max := 3
			if len(exchangePairs) < EXCHANGE_PAIRS_COUNT {
				max = len(exchangePairs)
			}
			list[idx].Pairs = exchangePairs[0:max]
		} else {
			list[idx].Pairs = []ExchangePair{}
		}

	}

	// TODO baseCurrency is "usd" for now for exchanges.
	// Implement volume_24h recalc later
	meta := Meta{page, perPage, totalRows, sort, search, baseCurrency}
	return &Exchanges{meta, list}, nil
}

func (s *Storage) GetExchangeByName(nam string, baseCurrency string) (*ExchangeDetails, error) {
	var (
		id, exchangeId               int
		volume24h, exchangeVolume24h NullFloat64
		name, title,
		exchangeName, exchangeTitle,
		exchangeCountry, linkType,
		linkTitle, linkURL string
	)

	baseCurrency, err := validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}

	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}

	dataValidFrom := s.exchangeDataValidFromDate()
	q := fmt.Sprintf(`
    SELECT t1.id,
      t1.name,
      t1.title,
      t1.country,
      t2.type,
      t2.title,
      t2.url,
      (SUM(data.volume_24h) / %[1]f) AS volume_24h
    FROM exchanges as t1
    LEFT JOIN exchange_links as t2
      ON t1.id=t2.exchange_id
    INNER JOIN token_exchange_data as data
      ON data.exchange_id = t1.id AND data.updated_at > $1
    WHERE t1.name = $2
    GROUP BY t1.id, t2.id`, basePrice)

	exchangeLinkRows, err := s.db.Query(q, dataValidFrom, nam)
	if err != nil {
		return nil, handleSQLError(err, "Exchange not found")
	}

	defer exchangeLinkRows.Close()

	var list = make([]Link, 0)
	for i := 0; exchangeLinkRows.Next(); i++ {
		exchangeLinkRows.Scan(
			&exchangeId,
			&exchangeName,
			&exchangeTitle,
			&exchangeCountry,
			&linkType,
			&linkTitle,
			&linkURL,
			&exchangeVolume24h,
		)
		if i == 0 {
			id = exchangeId
			name = exchangeName
			title = exchangeTitle
			volume24h = exchangeVolume24h
		}
		if linkURL != "" {
			el := Link{linkType, linkTitle, linkURL}
			list = append(list, el)
		}
	}

	pairsMap, err := s.GetPairsForExchanges([]string{strconv.Itoa(id)}, basePrice)
	if err != nil {
		return nil, handleSQLError(sql.ErrNoRows, "Pairs not found")
	}
	pairs := pairsMap[id]

	// TODO check different corner cases and correponding errors
	if list == nil {
		return nil, handleSQLError(sql.ErrNoRows, "Exchange not found")
	}
	exchange := Exchange{name, title, exchangeCountry, volume24h, list, pairs}
	return &ExchangeDetails{DetailsMeta{baseCurrency}, exchange}, nil
}

func (s *Storage) GetExchangePairs(nam string, sort string, search string, baseCurrency string) (*ExchangePairs, error) {
	// TODO handle 'exchange not found' error
	var (
		price     float64
		volume24h NullFloat64
		tokenTitle, fromSymbol, toSymbol,
		tokenSymbol string
		updatedAt       time.Time
		validSortParams = []string{"from_symbol", "to_symbol", "price", "volume_24h", "updated_at"}
	)

	safeSortParam, err := validateSortParam(validSortParams, sort)
	if err != nil {
		return nil, err
	}

	baseCurrency, err = validateBaseCurrency(baseCurrency)
	if err != nil {
		return nil, err
	}

	basePrice, err := s.getBasePriceFor(baseCurrency)
	if err != nil {
		return nil, err
	}

	searchWhere := "True"
	if search != "" {
		searchWhere = fmt.Sprintf(`(
			tok.title ILIKE '%%%s%%'
			OR (data.from_symbol || '/' || data.to_symbol) ILIKE '%%%s%%'
		)`, search, search)
	}

	dataValidFrom := s.exchangeDataValidFromDate()
	q := fmt.Sprintf(`
    SELECT
      tok.title,
      tok.symbol,
      data.from_symbol,
      data.to_symbol,
      (data.price / %[1]f) as price,
      (data.volume_24h / %[1]f) as volume_24h,
      updated_at
    FROM token_exchange_data AS data
    INNER JOIN exchanges AS ex
        ON ex.id = data.exchange_id
    INNER JOIN tokens AS tok
        ON tok.id = data.token_id
    WHERE ex.name = $1 AND data.updated_at > '%s' AND %s
    ORDER BY %s %s
    `, basePrice, dataValidFrom, searchWhere, safeSortParam.Name, safeSortParam.Direction)

	var list = []DetailedExchangePair{}
	pairRows, err := s.db.Query(q, nam)
	defer pairRows.Close()

	if err != nil {
		return nil, handleSQLError(err, "Pairs not found")
	}

	for pairRows.Next() {
		pairRows.Scan(
			&tokenTitle,
			&tokenSymbol,
			&fromSymbol,
			&toSymbol,
			&price,
			&volume24h,
			&updatedAt,
		)
		pair := DetailedExchangePair{tokenTitle, tokenSymbol, fromSymbol, toSymbol, price, volume24h, updatedAt}
		list = append(list, pair)
	}

	// TODO check different corner cases and correponding errors
	if list == nil {
		return nil, handleSQLError(sql.ErrNoRows, "Exchange not found")
	}

	return &ExchangePairs{PairsMeta{sort, search, baseCurrency}, list}, nil
}

func (s *Storage) GetPairsForExchanges(ids []string, basePrice float64) (map[int][]ExchangePair, error) {
	var (
		exchangeId           int
		fromSymbol, toSymbol string
		volume24h            NullFloat64
		price                float64
		updatedAt            time.Time
	)
	result := map[int][]ExchangePair{}
	if len(ids) == 0 {
		return result, nil
	}

	dataValidFrom := s.exchangeDataValidFromDate()
	// TODO display price for first token (from_symbol) for now.
	// (see tokens.id = data.token_id AND tokens.symbol = data.from_symbol condition).
	// We need to display price in "to_symbol" currency but in this case we need to
	// save two prices: price in USD and price in to_symbol currency.
	q := fmt.Sprintf(`
	  SELECT
        data.exchange_id,
        data.from_symbol,
        data.to_symbol,
        (data.volume_24h / %[1]f),
        (data.price / %[1]f),
        data.updated_at
      FROM token_exchange_data data
      INNER JOIN tokens
        ON tokens.id = data.token_id AND tokens.symbol = data.from_symbol
      WHERE data.exchange_id IN (%s) AND data.updated_at > $1
      ORDER BY data.volume_24h DESC
    `, basePrice, strings.Join(ids, ","))

	pairRows, err := s.db.Query(q, dataValidFrom)
	if err != nil {
		return nil, err
	}
	for i := 0; pairRows.Next(); i++ {
		pairRows.Scan(
			&exchangeId,
			&fromSymbol,
			&toSymbol,
			&volume24h,
			&price,
			&updatedAt,
		)
		el := ExchangePair{fromSymbol, toSymbol, price, volume24h, updatedAt}
		result[exchangeId] = append(result[exchangeId], el)
	}

	return result, nil
}

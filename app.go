package main

import (
	"flag"
	"go/build"
	"net/http"
	"os"

	log "github.com/Sirupsen/logrus"
	"github.com/ant0ine/go-json-rest/rest"
)

func main() {
	config := RegisterFlags()
	flag.Parse()

	// Debug flag controls how the logging is going to be set up.
	// We write log entries to the log file in production environments
	// and use stdout for development
	if config.Debug {
		log.SetLevel(log.DebugLevel)
	} else {
		logFile := build.Default.GOPATH + "/src/cherrydata/logs/error.log"
		f, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("Failed to open file: %v", err)
		}

		defer f.Close()
		log.SetOutput(f)

		log.SetLevel(log.InfoLevel)
	}

	storage, err := InitStorage(config)
	if err != nil {
		log.Fatalf("Failed to init storage: %v", err)
	}

	api := rest.NewApi()

	var stack = []rest.Middleware{
		&rest.CorsMiddleware{
			RejectNonCorsRequests: false,
			OriginValidator: func(origin string, request *rest.Request) bool {
				// TODO we should uncomment line below and move cors origin into config when we go in production
				// return origin == "https://tokenstatistics.com"
				return true
			},
			AllowedMethods:                []string{"GET"},
			AllowedHeaders:                []string{"authorization"},
			AccessControlAllowCredentials: true,
			AccessControlMaxAge:           3600,
		},
		// TODO: Check which mwares make sense for this project
		//		&rest.TimerMiddleware{},
		//		&rest.RecorderMiddleware{},
		//		&rest.RecoverMiddleware{},
		//		&rest.GzipMiddleware{},
		//		&rest.ContentTypeCheckerMiddleware{},
	}
	if config.Debug {
		// TODO Check how to set up log file name
		api.Use(&rest.AccessLogApacheMiddleware{})
	}
	api.Use(stack...)

	router, err := rest.MakeRouter(GetRoutes(&Ctx{storage})...)
	if err != nil {
		log.Fatalf("Failed to start the app: %v", err)
	}

	// TODO: Check how browser's fetch implementation works with Etags
	//	api.Use(&EtagMiddleware{storage.GetEtags()})
	//	api.Use(&CacheControlMiddleware{config.Cache.TTL})

	api.SetApp(router)

	log.Fatal(http.ListenAndServe(config.Port, api.MakeHandler()))
}

package main

type DB struct {
	User         string
	Password     string
	Hostname     string
	Name         string
	Port         string
	MaxIdleConns int
}

type Cache struct {
	Size int
}

type Images struct {
	BaseURL string
}

type Config struct {
	Port   string
	Debug  bool
	DB     DB
	Images Images
	Cache  Cache
}

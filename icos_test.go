package main

import (
	"testing"
	"time"
)

func TestGetICOWhere(t *testing.T) {
	mockNow := time.Date(2018, time.May, 1, 0, 0, 0, 0, time.UTC)
	testCases := [][]string{
		[]string{"", "", "True"},
		[]string{"btc", "", "(name ILIKE '%btc%')"},
		[]string{"btc", "upcoming", "(name ILIKE '%btc%') AND start_date > now()"},
		[]string{"", "upcoming", "start_date > now()"},
		[]string{"", "ongoing", "start_date < now() AND (end_date > now() OR end_date IS NULL) AND start_date > '2017-05-01T00:00:00Z'"},
		[]string{"", "finished", "end_date < now()"},
	}
	for _, testCase := range testCases {
		result, _ := getICOWhere(testCase[0], testCase[1], mockNow)
		if result != testCase[2] {
			t.Errorf("\n...expected: %v\n...got: %v", testCase[2], result)
		}
	}

	_, err := getICOWhere("btc", "not_valid_type", time.Now())
	if err.Error() != "Invald ICO type not_valid_type" {
		t.Errorf("\n...expected: %v\n...got: %v", "Invald ICO type not_valid_type", err.Error())
	}
}
